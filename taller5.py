#%%
import matplotlib.pyplot as plt 
import numpy as num

x_coordinates = [2, 3]
y_coordinates = [4, 4]

plt.plot(x_coordinates, y_coordinates)

plt.show()

num_segmentos = 35
rad = 4
cx = 4
cy = 0

angulo = num.linspace(0, 4*num.pi, num_segmentos+1)
x = rad * num.cos(angulo) + cx
y = rad * num.sin(angulo) + cy

plt.plot(x, y, color="red", markersize=1)
plt.plot(x, y, 'bo')

plt.title("ELIPSE")
plt.xlabel("X")
plt.ylabel("Y")
plt.gca().set_aspect('equal')
plt.grid()
plt.show()